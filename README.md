# AWS Default Credentials Switcher CLI Tool

> Allows Linux/Mac user to switch the default AWS credentials in use from the available profiles.

<img src="https://gitlab.com/supergoteam/component-storybook/raw/master/docs/images/super-go-team.logo.png" alt="Super GO Team" width="250"/>

## 😎 Motivation
Super GO Team and others deal with multiple AWS clients and accounts.

This package lets us easily switch default credentials in-use.

## 🤔 Other Methods
If you know your profile names, there are other ways to achieve this on temporary basis:

1. Add switch inline on commands: `--profile [profile-name]`
    - 🙂 useful for single commands
    - 🙁 easy to forget!
2. Set env var `export AWS_DEFAULT_PROFILE=[profile-name]` 
    - 🙂 allows you to switch default profile
    - 🙁 overrides [default] profile set in .aws/credentials
    - 🙁 relies on environment variables that can be lost across sessions/reboots


## ✅ Benefits
1. Easy menu to choose profile
2. Persistent switch of your default profile across sessions and reboots


## 💾 Install

```
npm i --global aws-default-credentials-switcher
```

## 💻 Run from CLI

> **WARNING**: This package will (locally) read and update your ~/.aws/credentials file. **MAKE A BACKUP BEFORE USE!**

### Select profile from menu:
```
aws-switch-credentials
```

### Specify profile as argument:
```
aws-switch-credentials [profile-name]
```

## 📝 Notes

- Assumes credentials file at `~/.aws/credentials` but can also set CREDENTIALS_FILE_LOCATION as environment variable
- Creates `~/.aws/credentials.awswitcher.firstbackup` on first run
- Creates `~/.aws/credentials.awswitcher.backup` on every run

## 📅 Project Status

V1 is a fast prototype with no refactoring.

V2 Planned Updates:
- Refactor codebase to seperate logic
- Improve formatting/colour output
- Better error handling

## 🦸🏼‍ Team 
- [Maintainer] Andy Hall <andy@superGOteam.com>
- [Maintainer] Wade Montague <wade@superGOteam.com>

## 📜 License
[MIT](https://choosealicense.com/licenses/mit/)