#!/usr/bin/env node
const fs = require('fs');
const homedir = require('os').homedir()

const setNewCredentials = (selectedProfile) => {
    const newContent = [];

    defaultCreds = creds[selectedProfile];
    creds['default'] = defaultCreds;

    Object.keys(creds).forEach(cred => {
        newContent.push('[' + cred + ']');
        Object.keys(creds[cred]).forEach(setting => {
            newContent.push(`${setting} = ${creds[cred][setting]}`);
        });
    });

    fs.writeFileSync(credsFile, newContent.join('\n'), { encoding: 'utf-8' });

    console.log('Credentials Updated to ' + selectedProfile + '!');
    console.log('---------------------------------------\n');
    process.exit();
}

const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
})

const credsFile = process.env.CREDENTIALS_FILE_LOCATION || homedir + '/.aws/credentials';

fs.copyFileSync(credsFile, credsFile + '.awswitcher.backup');

if (!fs.existsSync(credsFile + '.awswitcher.firstbackup')) {
    fs.copyFileSync(credsFile, credsFile + '.awswitcher.firstbackup');
}

const credsFileContent = fs.readFileSync(credsFile, { encoding: 'utf-8' });

console.log('\n---------------------------------------\nAWS Default Credentials Switcher\n---------------------------------------');

const creds = {};
var profile;

credsFileContent.split('\n').forEach(line => {
    if (line.substr(0, 1) == '[') {
        profile = line.replace(/[\[\]]/g, '');
        creds[profile] = { aws_access_key_id: '', aws_secret_access_key: '', region: 'us-west-1' }
    } else if (line.trim() != '') {
        const keyval = line.replace(/ /g, '').split('=');
        creds[profile][keyval[0]] = keyval[1];
    }
});

args = process.argv.slice(2);

if (args.length) {
    const profileName = args[0];
    if (creds[profileName]) {
        setNewCredentials(profileName);
    } else {
        console.log('Count not find passed profile: ', profileName);
        console.log('Please use list to select.\n');
    }
}


const credSelection = Object.keys(creds);

var question = 'Specify new default profile:\n';
credSelection.forEach((profile, idx) => {
    if (profile != 'default') {
        question += `${idx + 1}. ${profile}\n`;
    }
});
question += 'Specify: ';

readline.question(question, (selection) => {
    const selectedProfile = credSelection[selection - 1];
    console.log('');
    setNewCredentials(selectedProfile);
});
